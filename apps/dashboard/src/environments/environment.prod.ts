declare const HOST: string

export const environment = {
  production: true,
  host: HOST
};
