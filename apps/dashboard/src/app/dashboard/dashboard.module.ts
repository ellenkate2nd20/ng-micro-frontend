import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard.component';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    pathMatch: 'full'
  },
]

@NgModule({
  declarations: [DashboardComponent],
  imports: [RouterModule.forChild(routes)],
  providers: []
})
export class DashboardModule {}
