const ModuleFederationPlugin = require("webpack/lib/container/ModuleFederationPlugin");
const mf = require("@angular-architects/module-federation/webpack");
const path = require("path");
const dotenv = require('dotenv').config({path: __dirname + '/.env/.prod.env'}).parsed;
const DefinePlugin = require('webpack/lib/DefinePlugin');

const share = mf.share;

const sharedMappings = new mf.SharedMappings();
sharedMappings.register(
  path.join(__dirname, '../../tsconfig.base.json'),
  [/* mapped paths to share */]);

module.exports = {
  output: {
    uniqueName: "microFrontendDemo"
  },
  optimization: {
    runtimeChunk: false
  },   
  resolve: {
    alias: {
      ...sharedMappings.getAliases(),
    }
  },
  plugins: [
    new ModuleFederationPlugin({
      remotes: {
        // "admin": "admin@http://localhost:4300/remoteEntry.js",
        // "dashboard": "dashboard@http://localhost:4400/remoteEntry.js",
      },
      shared: share({
        "@angular/core": { singleton: true, strictVersion: true, requiredVersion: 'auto' }, 
        "@angular/common": { singleton: true, strictVersion: true, requiredVersion: 'auto' }, 
        "@angular/common/http": { singleton: true, strictVersion: true, requiredVersion: 'auto' }, 
        "@angular/router": { singleton: true, strictVersion: true, requiredVersion: 'auto' },

        ...sharedMappings.getDescriptors()
      })
    }),
    sharedMappings.getPlugin(),
    new DefinePlugin({
      ADM_HOST: JSON.stringify(dotenv.ADM_HOST),
      DB_HOST: JSON.stringify(dotenv.DB_HOST),
    })
  ],
};
