import { Routes } from "@angular/router";

export const APP_ROUTES: Routes = [
  {
    path: '',
    loadChildren: () => import('./home/home.module').then(m => m.HomeModule),
    pathMatch: 'full'
  },
  {
    path: 'about',
    loadChildren: () => import('./about/about.module').then(m => m.AboutModule),
    pathMatch: 'full'
  },
  // {
  //   path: 'admin',
  //   loadChildren: () => loadRemoteModule({
  //     remoteEntry: 'http://localhost:4300/remoteEntry.js',
  //     remoteName: 'admin',
  //     exposedModule: './Admin',
  //   }).then((m) => m.AppModule),
  // },
  // {
  //   path: 'dashboard',
  //   loadChildren: () => loadRemoteModule({
  //     remoteEntry: 'http://localhost:4400/remoteEntry.js',
  //     remoteName: 'dashboard',
  //     exposedModule: './Dashboard',
  //   }).then((m) => m.AppModule),
  // }
]