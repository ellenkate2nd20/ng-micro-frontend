import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    pathMatch: 'full'
  }
]

@NgModule({
  declarations: [HomeComponent],
  imports: [RouterModule.forChild(routes)],
  providers: []
})
export class HomeModule {}
