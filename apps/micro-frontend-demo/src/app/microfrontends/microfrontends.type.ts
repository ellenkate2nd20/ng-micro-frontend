import { LoadRemoteModuleOptions } from '@angular-architects/module-federation';

export type MicroFrontend = LoadRemoteModuleOptions & {
  displayName: string,
  routePath: string,
  ngModuleName: string
}