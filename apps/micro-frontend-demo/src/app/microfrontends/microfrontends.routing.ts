import { environment } from "../../environments/environment";
import { MicroFrontend } from "./microfrontends.type";

export const MICRO_FRONTEND_ROUTES: MicroFrontend[] = [
  {
    remoteEntry: environment.routing.admin,
    remoteName: 'admin',
    exposedModule: './Admin',
    displayName: 'Admin',
    routePath: 'admin',
    ngModuleName: 'AppModule'
  },
  {
    remoteEntry: environment.routing.dashboard,
    remoteName: 'dashboard',
    exposedModule: './Dashboard',
    displayName: 'Dashboard',
    routePath: 'dashboard',
    ngModuleName: 'AppModule'
  }
]