import { loadRemoteModule } from "@angular-architects/module-federation";
import { Injectable } from "@angular/core";
import { Routes } from "@angular/router";
import { APP_ROUTES } from "../app.routing";
import { MicroFrontend } from "./microfrontends.type";

@Injectable({
  'providedIn': 'root'
})
export class MicroFrontendService {
  buildRoutes(options: MicroFrontend[]): Routes {
    return [
      ...APP_ROUTES,
      ...options.map(o => ({
        path: o.routePath,
        loadChildren: () => loadRemoteModule(o).then(m => m[o.ngModuleName])
      }))
    ]
  }
}