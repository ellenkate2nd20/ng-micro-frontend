import { OnInit } from '@angular/core';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { MICRO_FRONTEND_ROUTES } from './microfrontends/microfrontends.routing';
import { MicroFrontendService } from './microfrontends/microfrontends.service';
import { MicroFrontend } from './microfrontends/microfrontends.type';

@Component({
  selector: 'mf-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'micro-frontend-demo'
  mfRoutes: MicroFrontend[] = MICRO_FRONTEND_ROUTES

  constructor(private router: Router, private mfService: MicroFrontendService) {}

  ngOnInit(): void {
    const routes = this.mfService.buildRoutes(this.mfRoutes)
    this.router.resetConfig(routes)
  }
}
