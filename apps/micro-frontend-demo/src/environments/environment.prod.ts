declare const ADM_HOST: string
declare const DB_HOST: string

export const environment = {
  production: true,
  routing: {
    admin: `${ADM_HOST}remoteEntry.js`,
    dashboard: `${DB_HOST}remoteEntry.js`
  }
};
