import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from './admin.component';

const routes: Routes = [
  {
    path: '',
    component: AdminComponent,
    pathMatch: 'full'
  }
]

@NgModule({
  declarations: [AdminComponent],
  imports: [RouterModule.forChild(routes)],
  providers: []
})
export class AdminModule {}
